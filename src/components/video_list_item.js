import React from 'react';


const VideoListItem = ({video, onVideoSelect}) => {
    // {video} wyzej jest rowne temu co ponizej. Zapis u gory to ES6 syntax
    // const video = props.video;

    const imageUrl = video.snippet.thumbnails.default.url;
    const title = video.snippet.title;
    // console.log(video);

    // const video = props.video;

    return (
        <li onClick={() => onVideoSelect(video)} className="list-group-item">
            <div className="video-list media">
                <div className="media-left">
                    <img src={imageUrl} alt="" className="media-object"/>
                </div>

                <div className="media-body">
                    <div className="media-heading">{title}</div>
                </div>
            </div>

        </li>
    );
};

export default VideoListItem;