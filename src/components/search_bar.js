import React, {Component} from 'react';

class SearchBar extends Component {
    constructor(props) {
        super(props);

        this.state = { term: '' };
    }

    render() {
        return (
            <div className="col-xs-12">
                <input style={{marginBottom: 10, marginTop: 10}}
                       className="col-xs-12 col-md-8"
                       placeholder="What do you want to watch?"
                       value={this.state.term}
                       onChange={event => this.onInputChange(event.target.value)}/>
            </div>
        )
    }



    onInputChange(term) {
        this.setState({term});
        this.props.onSearchTermChange(term);
    }
}

export default SearchBar;
