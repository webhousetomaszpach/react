import React from 'react';

const VideoDetail = ({video}) => {
    if (!video) {
        return <div className="loading col-md-8">Loading...</div>;
    }


    const videoId = video.id.videoId;
    // + videoId można zastapic przez ${videoId} i zamiane apostrofow
    // const url = 'https://www.youtube.com/embed/' + videoId;
    const url = `https://www.youtube.com/embed/${videoId}`;

    return (
        <div className="video-detail col-xs-12 col-md-8">
            <div className="embed-responsive embed-responsive-16by9">
                <iframe src={url} frameborder="0" className="embed-responsive-item"> </iframe>
            </div>

            <div className="details" style={{marginTop: 10, marginBottom: 10}}>
                <h1>{video.snippet.title}</h1>
                <div>{video.snippet.description}</div>
            </div>
        </div>
    )
};

export default VideoDetail;